%% clear
clear all global
restoredefaultpath

%% add the path to th_ptb
addpath('/home/th/git/th_ptb/') % change this to where th_ptb is on your system

%% initialize the PTB
th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/'); % change this to where PTB is on your system

%% get a configuration object
ptb_cfg = th_ptb.PTB_Config();

%% do the configuration
ptb_cfg.fullscreen = false;
ptb_cfg.window_scale = 0.2;
ptb_cfg.skip_sync_test = true;
ptb_cfg.hide_mouse = false;

%% get th_ptb.PTB object
ptb = th_ptb.PTB.get_instance(ptb_cfg);

%% init screen
ptb.setup_screen;

%% draw a rectangle with native commands
Screen('FillRect', ptb.win_handle, [0 0 0], [200 200 300 300]);
timestamp = ptb.flip(GetSecs()+1);

%% draw a rectangle with ptb.screen
ptb.screen('FillRect', th_ptb.constants.PTB_Colors.white, [300 300 400 400]);
timestamp = ptb.flip(GetSecs()+1);

%% draw a fixation cross
fix_cross = th_ptb.stimuli.visual.FixationCross();
ptb.draw(fix_cross);
ptb.flip();

%% draw some text
hello_world = th_ptb.stimuli.visual.Text('Hello World!');
ptb.draw(hello_world);
ptb.flip();

%% make the text bigger
hello_world.size = 90;
ptb.draw(hello_world);
ptb.flip();

%% draw a Gabor patch
gabor = th_ptb.stimuli.visual.Gabor(400);
gabor.frequency = .01;
gabor.sc = 60;
gabor.contrast = 120;

ptb.draw(gabor);
ptb.flip();

%% draw an image file
smiley = th_ptb.stimuli.visual.Image('smiley.png');

ptb.draw(smiley);
ptb.flip();

%% make the smiley smaller
smiley.scale(0.5);

ptb.draw(smiley);
ptb.flip();

%% move the smiley
smiley.move(150, 200);

ptb.draw(smiley);
ptb.flip();